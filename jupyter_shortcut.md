# Shortcuts
Action | key
---| ---
Enter edit mode (Green) | Enter
Enter sheet mode (Blue) | Esx
Find in cell | f
cut cell | x
copy cell| c
paste cell | v
insert new cell above | a
insert new cell below | b
Change cell to markdown | m
Change cell to code | y
Run cell | Shift+Enter or Control+Enter