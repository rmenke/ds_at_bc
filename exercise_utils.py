from sklearn import metrics
import numpy as np
import pandas as pd
from itertools import permutations
import matplotlib.pyplot as plt
from sklearn.utils.multiclass import unique_labels


def _remap_array(mapping, y, original_mapping):
    new = "".join(y)
    for i in range(len(mapping)):

        new = new.replace(mapping[i][0],mapping[i][1])

    for i in range(len(original_mapping)):
        new = new.replace(original_mapping[i][1],original_mapping[i][0])
    out = np.array([int(x) for x in new])
    return out

def match_labels(y_true, y_pred):
    """Find the best match between predicted labels and the ground truth

    Simply permutes all the possible combinations and returns the best remapping
    Due to the naive implimentation of the algorith, this is limited to a small number of labels

    Args:
        y_true (np.array): Truth. Assumes labels are in 0,...n where n <= 5
        y_pred (np.array): Predicted labels, in the same range as y_true

    Returns:
        np.array: remapped ersion of y_pred
    """

    x = len(set(y_true))
    if x > 5:
        raise ValueError("Too many combinations, this will not work")        

    result = {}
    mappings = [tuple(zip('0123456789'[:x], p)) for p in permutations('abcdefghijk'[:x])]
    for mapping in mappings:
        out = _remap_array(mapping, y_pred.astype(str), mappings[0] )              
        result[mapping] = np.mean(y_true== out)

    ideal_mapping = pd.Series(result).idxmax()
    out = _remap_array(ideal_mapping, y_pred.astype(str), mappings[0])    
    return out
            


def eval_clustering(y_true, y_pred):    
    print("Homogeneity: %0.3f" % metrics.homogeneity_score(y_true,y_pred ))
    print("Completeness: %0.3f" % metrics.completeness_score(y_true, y_pred))
    print("V-measure: %0.3f" % metrics.v_measure_score(y_true, y_pred))
    print("Adjusted Rand-Index: %.3f"
          % metrics.adjusted_rand_score(y_true, y_pred))

    
    

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues,
                          figsize=()):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if len(figsize) == 0:
        figsize=(len(classes)+2,len(classes)+2)
    
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = metrics.confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    #classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots(figsize=figsize)
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


def get_bank_data(mincount=2000):
    "Get complaints data, discarding rare points"
    df = pd.read_csv('https://data.consumerfinance.gov/api/views/s6ew-h6mp/rows.csv?accessType=DOWNLOAD')
    df = df[['Product', 'Consumer complaint narrative']].dropna()
    df.columns = ['category', 'data']
    g = df.groupby('category').count() > mincount
    categories = g[g.data].index
    df = df[df.category.isin(categories)]
    df['subject'] = pd.Categorical(df.category)
    df['subject'] = df['subject'].cat.codes
    return df