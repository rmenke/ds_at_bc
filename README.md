# Getting started
Download this repository into a new folder. 

You'll need to install sklearn and pandas. If you already have it just use the versions you have, unless there is a problem.

    pip install sklearn==0.20.1
    pip install pandas==0.24.1

## Exercises
- The first exercise is on clustering of texts
- The second exercise tries to classify texts